package activities;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VistaClienteConexion extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField ip_text;
	private JTextField port_text;
	private JTextField usuari_text;
	private JComboBox<String> comboBox;

	private VistaClienteChat chat = new VistaClienteChat();
	private JButton btnConectar;
	private JButton btnDesconectar;
	
	Cliente cliente;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaClienteConexion frame = new VistaClienteConexion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaClienteConexion() {
		setTitle("Conexi\u00F3n");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JLabel lblNewLabel = new JLabel("Conexi\u00F3n usuario");
		lblNewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 24));
		panel.add(lblNewLabel);

		JSeparator separator = new JSeparator();
		panel.add(separator);

		JLabel lblNewLabel_1 = new JLabel("IP");
		lblNewLabel_1.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNewLabel_1.setFont(new Font("Dialog", Font.BOLD, 16));
		panel.add(lblNewLabel_1);

		ip_text = new JTextField();
		panel.add(ip_text);
		ip_text.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Port");
		lblNewLabel_2.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNewLabel_2.setFont(new Font("Dialog", Font.BOLD, 16));
		panel.add(lblNewLabel_2);

		port_text = new JTextField();
		panel.add(port_text);
		port_text.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("Usuari");
		lblNewLabel_3.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNewLabel_3.setFont(new Font("Dialog", Font.BOLD, 16));
		panel.add(lblNewLabel_3);

		usuari_text = new JTextField();
		panel.add(usuari_text);
		usuari_text.setColumns(10);

		JPanel panel_1 = new JPanel();
		panel_1.setPreferredSize(new Dimension(10, 50));
		panel.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));

		btnConectar = new JButton("Conectar");
		btnConectar.setActionCommand("conectar");
		panel_1.add(btnConectar);
		btnConectar.addActionListener(this);

		btnDesconectar = new JButton("Desconectar");
		panel_1.add(btnDesconectar);
		btnDesconectar.addActionListener(this);

		JPanel panel_2 = new JPanel();
		panel.add(panel_2);

		comboBox = new JComboBox<String>();
		comboBox.setActionCommand("comboBox");
		comboBox.setEnabled(false);
		panel_2.add(comboBox);
		comboBox.addActionListener(this);

		pack();
	}

	public String getIp_textText() {
		return ip_text.getText();
	}

	public void setIp_textText(String text) {
		ip_text.setText(text);
	}

	public String getPort_textText() {
		return port_text.getText();
	}

	public void setPort_textText(String text_1) {
		port_text.setText(text_1);
	}

	public String getUsuari_textText() {
		return usuari_text.getText();
	}

	public void setUsuari_textText(String text_2) {
		usuari_text.setText(text_2);
	}

	public String getReceptor() {
		return comboBox.getSelectedItem().toString();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String accion = arg0.getActionCommand();

		switch (accion) {
		case "conectar":
			ip_text.setEnabled(false);
			port_text.setEnabled(false);
			usuari_text.setEnabled(false);
			comboBox.setEnabled(true);
			
			btnConectar.setText("Actualizar");
			
			

			try {
				cliente = new Cliente(getIp_textText(), getPort_textText(), getUsuari_textText(),chat);
				chat.setClienteEmisor(cliente);

				String listaConectados = cliente.realizarConexion();
				actualizarLista(listaConectados);
				cliente.ejecutaCliente();


			} catch (IOException e) {	
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;

		case "Desconectar":
			
			btnConectar.setText("Conectar");
			ip_text.setEnabled(true);
			ip_text.setText("");
			port_text.setEnabled(true);
			port_text.setText("");
			usuari_text.setEnabled(true);
			usuari_text.setText("");
			comboBox.setEnabled(false);

			chat.setVisible(false);
			
			//DESDE EL BOTON DESCONECTAR LLAMO AL METODO DEL CLIENTE QUE REALIZA LA DESCONEXION.
			cliente.realizarDesconexion();
			

			break;

		case "comboBox":

			chat.setVisible(true);
			String usuarioReceptor = comboBox.getSelectedItem().toString();
			String usuarioEmisor = usuari_text.getText().toString();
			chat.setReceptorCabeceraMensaje(usuarioReceptor);
			chat.setEmisorCabeceraMensaje(usuarioEmisor);
			chat.setTitleChat(usuarioReceptor);
			break;

		default:
			break;
		}

	}

	public void actualizarLista(String lista) {
		String[] listaActualizada = lista.split("#");
		comboBox.setModel(new DefaultComboBoxModel<String>(listaActualizada));
	}

}
