package activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Cliente implements Runnable {

	String ip;
	String port;
	String usuari;
	int portInt;

	String listaConectados;
	Socket socket;
	PrintWriter write;
	BufferedReader leer;
	InetAddress ia;
	VistaClienteChat chat;

	public Cliente(String ip, String port, String usuari, VistaClienteChat chat) throws IOException {
		this.ip = ip;
		this.port = port;
		this.usuari = usuari;
		this.chat = chat;
		portInt = Integer.parseInt(port);
		ia = InetAddress.getByName(ip);
		socket = new Socket(ia, portInt);
		// chat = new VistaClienteChat();
		write = new PrintWriter(socket.getOutputStream(), true);
		leer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	}

	public String realizarConexion() throws UnknownHostException {

		try {

			write.println("666*conexion#" + usuari);
			listaConectados = leer.readLine();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaConectados;

	}

	//CREO EL METODO PARA ENVIAR LA CABECERA DE DESCONEXION AL SERVIDOR.
	public void realizarDesconexion() {

		write.println("777*desconexion#" + usuari);

	}

	public void enviarMensaje(String mensaje) throws UnknownHostException {

		write.println(mensaje);
		String[] mensajeEnvio = mensaje.split("#");
		chat.ponerMensajeEnCliente(mensajeEnvio[1]);

	}

	public void recibirMensaje() throws IOException {
		String mensajeRecibido;
		while ((mensajeRecibido = leer.readLine()) != null) {
			System.out.println(mensajeRecibido);
			chat.setClienteEmisor(this);
			chat.ponerMensajeEnCliente(mensajeRecibido);
			chat.setVisible(true);
			String[] split = mensajeRecibido.split(":");
			String destinatario = split[0];
			chat.setEmisorCabeceraMensaje(usuari);
			chat.setReceptorCabeceraMensaje(destinatario);
			chat.setTitleChat(destinatario);

		}

	}

	@Override
	public void run() {
		try {
			recibirMensaje();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Metodo para escuchar mensaje
	}

	public void ejecutaCliente() throws IOException {
		Thread hiloEscucha = new Thread(this);
		hiloEscucha.start();
	}
}
