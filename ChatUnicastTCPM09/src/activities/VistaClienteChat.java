package activities;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.UnknownHostException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VistaClienteChat extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField mensaje_text;
	private JTextArea area_chat;
	private JButton btnEnviar;
	private  String userEmisor;
	private  String userReceptor;
	private Cliente clienteEmisor;
	
	String conversacion = "";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaClienteChat frame = new VistaClienteChat();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaClienteChat() {
		setResizable(false);
		setTitle("CHAT");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(230, 250));
		contentPane.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JScrollPane scroll = new JScrollPane();
		scroll.setPreferredSize(new Dimension(100, 200));
		panel.add(scroll);

		area_chat = new JTextArea();
		area_chat.setMargin(new Insets(5, 5, 5, 5));
		area_chat.setWrapStyleWord(true);
		area_chat.setLineWrap(true);
		area_chat.setEditable(false);
		scroll.setViewportView(area_chat);

		JPanel panel_1 = new JPanel();
		panel.add(panel_1);

		mensaje_text = new JTextField();
		panel_1.add(mensaje_text);
		mensaje_text.setPreferredSize(new Dimension(100, 25));
		mensaje_text.setColumns(10);

		btnEnviar = new JButton("Enviar");
		panel_1.add(btnEnviar);
		btnEnviar.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnEnviar.addActionListener(this);

		pack();
	}

	public String getMensaje_textText() {
		return mensaje_text.getText();
	}

	public void setMensaje_textText(String text) {
		mensaje_text.setText(text);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String accion = arg0.getActionCommand();

		switch (accion) {
		case "Enviar":
			String mensajeEnviado = getMensaje_textText();
			String mensajeEnviar = userReceptor+"#"+userEmisor+": "+mensajeEnviado;
			try {
				clienteEmisor.enviarMensaje(mensajeEnviar);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			break;

		default:
			break;
		}

	}
	
	public  void setEmisorCabeceraMensaje(String usuarioEmisor) {
		this.userEmisor = usuarioEmisor;
	}
	
	public  void setReceptorCabeceraMensaje(String usuarioReceptor) {
		this.userReceptor = usuarioReceptor;
	}
	
	public void setClienteEmisor(Cliente cliente) {
		this.clienteEmisor = cliente;
	}

	public void ponerMensajeEnCliente(String mensajeRecibido) {
		conversacion = conversacion + mensajeRecibido+"\n";
		area_chat.setText(conversacion);
	}
	
	public void setTitleChat(String title) {
		this.setTitle(title);
	}
}
