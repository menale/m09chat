package activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;

public class Servidor {

	private static HashMap<String, Socket> llistaUsers = new HashMap<>();

	public static void main(String[] args) throws IOException {
		int serverPort = 4321;
		ServerSocket serverSocket = new ServerSocket(serverPort);

		while (true) {
			new Thread(new ServidorRunnable(serverSocket.accept())).start();
		}

	}

	public static void addUser(String username, Socket clientSocket) {

		llistaUsers.put(username, clientSocket);
	}

	//CREO EL METODO PARA ELIMINAR UN USUARIO DEL HASHMAP.
	public static void removeUser(String username) {
		llistaUsers.remove(username);
	}

	public static HashMap<String, Socket> getLlistaUsers() {
		return llistaUsers;
	}

}

class ServidorRunnable implements Runnable {

	private Socket clientSocket;
	private final static String IDENTIFICADOR_CONEXION = "666*conexion";
	//CREO ESTA CONSTANTE QUE CONTIENE LA CABECERA DE DESCONEXION.
	private final static String IDENTIFICADOR_DESCONEXION = "777*desconexion";
	private HashMap<String, Socket> llistaUsers;

	public ServidorRunnable(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	@Override
	public void run() {
		llistaUsers = Servidor.getLlistaUsers();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			PrintWriter out = null;
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);
				String[] mensaje = inputLine.split("#");
				if (mensaje[0].equals(IDENTIFICADOR_CONEXION)) {
					Servidor.addUser(mensaje[1], clientSocket);
					mostrarListaUsuarios();

					// CREO ESTE ELSE IF, SI RECIBE UN PAQUETE CON LA CABECERA DE DESCONEXION BORRA
					// EL USUARIO QUE LO ENVIA DEL HASMPAP
				} else if (mensaje[0].equals(IDENTIFICADOR_DESCONEXION)) {
					System.out.println("borrar de la lista usuario: " + mensaje[1]);
					Servidor.removeUser(mensaje[1]);
				}

				else {
					Socket socketDestino = obtenerSocketDestino(mensaje[0]);
					out = new PrintWriter(socketDestino.getOutputStream(), true);
					System.out.println("Mensaje enviado a " + mensaje[0]);
					out.println(mensaje[1]);
				}
			}

			in.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Un usuario ha salido");
		}
	}

	private void mostrarListaUsuarios() throws IOException {
		PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
		String llistaUsersCombo = "";
		for (String nom : llistaUsers.keySet()) {
			llistaUsersCombo = llistaUsersCombo + nom + "#";
		}
		out.println(llistaUsersCombo);
	}

	private Socket obtenerSocketDestino(String nomuser) throws UnknownHostException, IOException {
		Socket socket = null;

		for (String nom : llistaUsers.keySet()) {
			if (nom.equals(nomuser)) {
				socket = llistaUsers.get(nomuser);
				return socket;

			}
		}
		return socket;
	}
}
